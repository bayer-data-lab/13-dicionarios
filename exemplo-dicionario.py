pessoa = {
    'nome': 'Gabriel',
    'idade': 24,
    'altura': 1.81,
    'peso': 79.9,
    'funcionario_bayer': False,
    'top_3_filmes': ['Clube da luta',
                     'Rocky Balboa',
                     'Watchman'],
    'top_amiga': {
        'nome': 'Natalia Valentin',
        'top_3_filmes': [
            'Star wars',
            'Kill bill',
            'Her'],
        'idade': 25
    }
}

pessoa.update({'desenho_favorito': 'Irmão do Jorel'})

print(pessoa.get('top_3_filmes')[0])


filmes = [
    {
        "nome": "xablau",
        "autor": "xpto"
    },
    {
        "nome": "xablau 2",
        "autor": "xpto 2"
    }
]

print(filmes[0])
print(filmes[1])
